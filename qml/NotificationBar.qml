import QtQuick 2.0
import Sailfish.Silica 1.0

MouseArea {
    id: root
    width: parent.width
    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
    }
    height: box.height
    visible: opacity > 0
    property var clickedCallback: undefined
    property string clickText: ""
    property ApplicationWindow appWindow: parent
    opacity: 0
    Behavior on opacity { FadeAnimator {} }

    function rotate () {
        anchors.top = parent.top
        anchors.left = parent.left
        anchors.right = parent.right
        anchors.bottom = undefined
        transformOrigin = Item.Center
        width = parent.width
        rotation = 0
        switch(app.orientation) {
            case Orientation.Portrait:
                break
            case Orientation.Landscape:
                anchors.top = parent.bottom
                anchors.left = undefined
                anchors.right = parent.right
                width = parent.height
                transformOrigin = Item.TopRight
                rotation = 90
                break
            case Orientation.PortraitInverted:
                anchors.top = parent.bottom
                anchors.left = parent.left
                anchors.right = parent.right
                width = parent.width
                transformOrigin = Item.Top
                rotation = 180
                break
            case Orientation.LandscapeInverted:
                anchors.top = parent.bottom
                anchors.left = parent.left
                anchors.right = undefined
                width = parent.height
                transformOrigin = Item.TopLeft
                rotation = 270
                break
        }
    }

    Component.onCompleted: {
        rotate()
        appWindow.orientationChanged.connect(rotate)
    }

    Rectangle {
        id: box
        width: parent.width
        height: column.height
        color: Theme.rgba(Theme.highlightDimmerColor, 0.8)

        Column {
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            Item {
                width: parent.width
                height: column.spacing * 2
            }

            Item {
                width: parent.width
                height: Math.max(icon.height, label.height)

                MonochromeIcon {
                    id: icon
                    anchors {
                        left: parent.left
                        verticalCenter: label.verticalCenter
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.paddingLarge
                    }
                    source: "image://theme/icon-m-about"
                    color: Theme.highlightColor
                }

                Label {
                    id: label
                    anchors {
                        left: icon.right
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                        leftMargin: Theme.paddingLarge
                    }
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    wrapMode: Text.WordWrap
                    truncationMode: TruncationMode.Fade
                    maximumLineCount: 5
                }

            }


            Label {
                id: noteLabel
                width: parent.width - 2 * Theme.horizontalPageMargin
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                horizontalAlignment: Text.AlignRight
                visible: text != ""
                text: root.clickedCallback ? (root.clickText || "") :
                    qsTranslate("notification-bar", "Tap to hide")
                truncationMode: TruncationMode.Fade
            }

            Item {
                width: parent.width
                height: column.spacing * 2
            }

        }

    }

    Timer {
        id: timer
        repeat: false
        onTriggered: reset()
    }

    function show (text, iconpath, callback, clickText) {
        console.debug("showing notification bar with text %1".arg(text))
        if (callback) { root.clickedCallback = callback }
        if (clickText) { root.clickText = clickText }
        label.text = text
        icon.source = iconpath || "image://theme/icon-m-about"
        opacity = 1
        timer.interval = 7000
        timer.start()
    }

    function success(text, callback, clickText) {
        show(text, "image://theme/icon-m-accept", callback, clickText)
    }

    function info(text, callback, clickText) {
        show(text, "image://theme/icon-m-about", callback, clickText)
    }

    function warning(text, callback, clickText) {
        show(text, "image://theme/icon-m-dismiss", callback, clickText)
    }

    function error(text, callback, clickText) {
        show(text, "image://theme/icon-m-dismiss", callback, clickText)
    }

    function reset () {
        hide()
        timer.stop()
        clickedCallback = undefined
        clickText = ""
    }

    function hide () {
        opacity = 0
    }

    onClicked: {
        if(clickedCallback) {
            hide()
            clickedCallback()
        }
        reset()
    }

}
