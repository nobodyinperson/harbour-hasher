import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: root

    property Item hashAlgorithmItem

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                text: qsTranslate("first-page","About Hasher")
                onClicked: app.pageStack.push("AboutPage.qml")
            }
            MenuItem {
                text: qsTranslate("menu","Select File")
                onClicked: ((root.hashAlgorithmItem||{})
                    .lastFocusedBytesInputField||{}).fileMode = true
                enabled: (root.hashAlgorithmItem||{})
                    .lastFocusedBytesInputField !== undefined
                        && !hashDisplay.busy && ((root.hashAlgorithmItem||{})
                    .lastFocusedBytesInputField||{}).fileMode !== true
            }
        }

        ViewPlaceholder {
            id: viewplaceholder
            enabled: !python.importing && !python.ready
            text: qsTranslate("viewplaceholder", "No Codecs or Algorithms")
            hintText: python.imported ?
                qsTranslate("viewplaceholder", "Backend didn't send any") :
                qsTranslate("viewplaceholder", "Backend couldn't be loaded")
        }

        Column {
            visible:  !viewplaceholder.enabled
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: qsTranslate("pageheader","Hasher")
            }

            HashAlgorithmItem {
                id: hashAlgorithmItem
                codecsModel: python.codecsModel
                algorithmsJson:
                    JSON.stringify(python.hashAlgorithmsComposite || "{}")
                algorithmsModel: python.hashAlgorithmsCompositeModel
                enabled: !hashDisplay.busy
                onTextCopied: {
                    notificationBar.success(qsTranslate("notification-bar",
                        "%1 copied to clipboard").arg(description))
                }
                onInvalidItemSelected: {
                    notificationBar.error(qsTranslate("notification-bar",
                        "invalid selection"))
                }
                Component.onCompleted: {
                    root.hashAlgorithmItem = hashAlgorithmItem
                }
            }

            SectionHeader {
                text: qsTranslate("section", "Result")
            }

            HashDisplayItem {
                id: hashDisplay
                codecsModel: python.codecsModel
                algorithmParams: hashAlgorithmItem.algorithmParams
                initialDisplayEncodingElement:
                    function(e){ return e.name == bridgeEncoding }
                onTextCopied: {
                    notificationBar.success(qsTranslate("notification-bar",
                        "%1 copied to clipboard").arg(description))
                }
                onDisplayTextChanged: {
                    cover.setText(displayText)
                    cover.stringMode = hashDisplay.stringMode
                }
                Component.onCompleted: {
                    app.hashDisplayItem = hashDisplay
                }
            }

            Rectangle {
                width: parent.width
                height: Theme.paddingLarge
                opacity: 0
            }

        }
    }
}


