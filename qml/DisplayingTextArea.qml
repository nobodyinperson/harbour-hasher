import QtQuick 2.0
import Sailfish.Silica 1.0

TextArea {
    property bool displaySeparateBytes: false
    property bool error: false
    property var busy: false
    property var monoFontFamily: Theme.fontFamily

    font.family: displaySeparateBytes ? monoFontFamily : Theme.fontFamily
    font.pixelSize: Theme.fontSizeTiny +
        (Theme.fontSizeMedium - Theme.fontSizeTiny)
        / ( 1 + (text.length * (1 + 10 * displaySeparateBytes) ) / 1000 )
    enabled: !error && !busy
    wrapMode: displaySeparateBytes ? TextEdit.WordWrap : TextEdit.WrapAnywhere
    width: parent.width

    BusyIndicator {
        visible: running
        running: busy
        anchors.centerIn: parent
        size: BusyIndicatorSize.Medium
    }

    function clear () {
        text = ""
    }

    function update () {
        if(visible) {
            console.debug("Nothing to update...")
        }
    }

    // onVisibleChanged:              update()
    onDisplaySeparateBytesChanged: update()

    Component.onCompleted: {
        // find first monospace font
        Qt.fontFamilies().some(function(family){
            if(family.toLowerCase().indexOf("mono") != -1) {
                monoFontFamily = family
                return true
            }
        })
    }

}
