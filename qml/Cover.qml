import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

CoverBackground {
    id: cover

    property var text: ""
    property bool showText: true
    property bool stringMode: false

    function setText (t) {
        text = t
    }

    Label {
        id: coverLabel
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        width: parent.width - 2 * Theme.paddingMedium
        height: 0.85 * parent.height - 2 * Theme.paddingMedium
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            bottomMargin: Theme.paddingMedium
            topMargin: Theme.paddingMedium
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
        visible: showText
        wrapMode: text.indexOf(" ")!=-1 ? Text.WordWrap : Text.WrapAnywhere
        font.family:
            (wrapMode == Text.WordWrap && !stringMode) ?
                app.monoFontFamily : Theme.fontFamily
        elide: Text.ElideRight
        font.pixelSize: Theme.fontSizeTiny +
            (Theme.fontSizeHuge- Theme.fontSizeTiny)
            / ( 1 + (text.length) / 40 )
        text: cover.text
    }

    Image {
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
        anchors.fill: parent
        clip: true
        source: "../images/cover-background.svg"
        opacity: 0.1
    }

    CoverActionList {
        id: actionList
        CoverAction {
               iconSource: showText ?
                   "image://theme/icon-cover-cancel" :
                   "image://theme/icon-cover-sync"
            onTriggered: showText = !showText
        }
    }

}

