import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: root
    height: column.height
    width: parent.width

    property var slider: undefined
    property var textField: undefined

    property var sliderLabel: undefined
    property var textFieldLabel: undefined
    property var textFieldPlaceholderText: undefined

    property var defaultValue: undefined

    property var minimum: undefined
    property var maximum: undefined

    readonly property var _intLimits: IntValidator {}

    property var validator: IntValidator {
        bottom: minimum === undefined ? _intLimits.bottom : minimum
        top: maximum === undefined ? _intLimits.top : maximum
    }

    property bool sliderMode: true

    property int value: (slider && textField) ?
        (sliderMode ? slider.slowValue : textField.slowValue) : undefined

    onSliderModeChanged: {
        if(sliderMode) {
            if(intSlider.maximumValue < value) {
                intSlider.maximumValue =
                    Math.round(value / 100) * 100 * 2
            }
            intSlider.value = value
            intSlider.slowValue = value
        } else {
            intTextField.text = value
            intTextField.slowValue = value
        }
    }

    Column {
        id: column
        spacing: Theme.paddingMedium
        width: parent.width

        Slider {
            id: intSlider
            minimumValue: root.validator.bottom
            maximumValue: root.validator.top
            width: parent.width
            property int slowValue: undefined
            valueText: value
            stepSize: 1
            visible: sliderMode
            label: sliderLabel || qsTranslate("slider-label", "Number")
                 + ((validator.bottom < value && value < validator.top ) ?
                    " - %1".arg(qsTranslate("first-page",
                    "long press to enter manually")) : "")
            onPressAndHold: { sliderMode = false }
            onDownChanged: {
                if(!down) {
                    if(!(validator.bottom<=value&&value<=validator.top)) {
                        value = (Math.abs(validator.top-value) <
                            Math.abs(value-validator.bottom)) ?
                            validator.top : validator.bottom
                    }
                    slowValue = value
                }
            }
            Component.onCompleted: {
                if(defaultValue !== undefined) {
                    if(validator.bottom <= defaultValue
                        && defaultValue <= validator.top) {
                        intSlider.value = defaultValue
                    }
                } else {
                    intSlider.value = validator.bottom
                }
                slowValue = value
                slider = intSlider
            }
        }

        TextField {
            id: intTextField
            visible: ! sliderMode
            width: parent.width
            label: textFieldLabel || qsTranslate("textfield-label","Number")
            placeholderText: textFieldPlaceholderText ||
                qsTranslate("textfield-placeholder", "Enter a positive number")
            validator: root.validator
            property int slowValue: undefined
            property int value: Number(text)
            onPressAndHold: { sliderMode = true }
            onFocusChanged: {
                if(!focus) {
                    if(!(validator.bottom<=value&&value<=validator.top)) {
                        value = (Math.abs(validator.top-value) <
                            Math.abs(value-validator.bottom)) ?
                            validator.top : validator.bottom
                    }
                    slowValue = value
                }
            }
            Component.onCompleted: {
                text = validator.bottom
                slowValue = Number(text)
                textField = intTextField
            }
        }

    }

}
