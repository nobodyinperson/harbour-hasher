import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

ApplicationWindow
{
    id: app
    property bool loading: false
    property bool portrait: app.orientation & Orientation.PortraitMask
    initialPage: Component { FirstPage { } }
    cover: Cover { id: cover }
    property var version: undefined
    property var monoFontFamily: Theme.fontFamily

    NotificationBar {
        id: notificationBar
        appWindow: app
    }

    property var hashDisplayItem

    Item {
        id: busyItem
        height: Math.max(busyIndicator.height, hashProgressBar.height)

        function rotate () {
            anchors.top = parent.top
            anchors.left = parent.left
            anchors.right = parent.right
            anchors.bottom = undefined
            transformOrigin = Item.Center
            width = parent.width
            rotation = 0
            switch(app.orientation) {
                case Orientation.Portrait:
                    break
                case Orientation.Landscape:
                    anchors.top = parent.bottom
                    anchors.left = undefined
                    anchors.right = parent.right
                    width = parent.height
                    transformOrigin = Item.TopRight
                    rotation = 90
                    break
                case Orientation.PortraitInverted:
                    anchors.top = parent.bottom
                    anchors.left = parent.left
                    anchors.right = parent.right
                    width = parent.width
                    transformOrigin = Item.Top
                    rotation = 180
                    break
                case Orientation.LandscapeInverted:
                    anchors.top = parent.bottom
                    anchors.left = parent.left
                    anchors.right = undefined
                    width = parent.height
                    transformOrigin = Item.TopLeft
                    rotation = 270
                    break
            }
        }

        Component.onCompleted: {
            rotate()
            app.orientationChanged.connect(rotate)
        }

        BusyIndicator {
            id: busyIndicator
            visible: running
            running: hashDisplayItem.busy
            anchors {
                top: parent.top
                left: parent.left
                leftMargin: Theme.horizontalPageMargin
                topMargin:  Theme.horizontalPageMargin
            }
            size: BusyIndicatorSize.Medium
        }

        ProgressBar {
            id: hashProgressBar
            visible: busyIndicator.visible && value
            anchors {
                left: busyIndicator.right
                right: parent.right
                rightMargin: parent.width / 4
                verticalCenter: busyIndicator.verticalCenter
            }
            minimumValue: 0
            maximumValue: 1
            value: (app.hashDisplayItem||{}).hashProgress||0
        }
    }

    Python {
        id: python
        onError: {
            notificationBar.warning(
                qsTranslate("app","Python error:\n%1").arg(traceback),
                function () { app.pageStack.push("ErrorPage.qml",
                    {"message": traceback})
                },
                qsTranslate("notification-bar", "Tap to show")
            )
        }
        Component.onCompleted: {
            setHandler("app-version",function(v){version = v})
            setHandler("error",function(text){notificationBar.error(text)})
            setHandler("warning",function(text){notificationBar.warning(text)})
            setHandler("info",function(text){notificationBar.info(text)})
            setHandler("success",function(text){notificationBar.success(text)})
        }
    }

    Component.onCompleted: {
        // find first monospace font
        Qt.fontFamilies().some(function(family){
            if(family.toLowerCase().indexOf("mono") != -1) {
                monoFontFamily = family
                return true
            }
        })
    }

}


