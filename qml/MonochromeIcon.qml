import QtQuick 2.0
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

// adapted from https://doc.qt.io/qt-5/qml-qtgraphicaleffects-coloroverlay.html

Item {
    id: root

    property string source
    property string color

    width: image.width
    height: image.height

    Image {
        id: image
        source: root.source
        visible: false
    }

    ColorOverlay {
        id: overlay
        color: root.color || Theme.primaryColor
        source: image
        anchors.fill: parent
    }
}

