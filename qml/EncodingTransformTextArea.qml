import QtQuick 2.0
import Sailfish.Silica 1.0

DisplayingTextArea {
    property var sourceText: ""
    property var sourceEncoding: undefined
    property var displayEncoding: undefined

    property bool active: true

    property var _currentlyProcessingText: undefined

    function update () {
        if(!active || _currentlyProcessingText == sourceText
            || !sourceEncoding || !displayEncoding) {
            return
        }
        _currentlyProcessingText = sourceText
        console.debug(
            "Telling Python to transform " +
            "'%1' from '%2' to '%3'"
            .arg(sourceText)
            .arg(sourceEncoding)
            .arg(displayEncoding)
        )
        busy = true
        python.call("python.app.transform",
            [sourceText, sourceEncoding, displayEncoding],
            function(transformed){
            if(transformed === undefined) {
                console.warn(("Python obviously had a problem " +
                    "transforming '%1' from %2 to %3").arg(sourceText)
                        .arg(sourceEncoding).arg(displayEncoding))
                error = true
                text = ""
            } else {
                console.warn(("Python says " +
                    "transforming '%1' from %2 to %3 yields '%4'")
                        .arg(sourceText).arg(sourceEncoding)
                        .arg(displayEncoding)
                        .arg(transformed))
                error = false
                text = transformed
            }
            busy = false
            _currentlyProcessingText = undefined
        })
    }

    onActiveChanged:               {
        // console.debug("updating because active changed to %1".arg(active))
        update()
        }
    onSourceTextChanged:           {
        // console.debug("updating because sourceText changed to %1"
        //     .arg(sourceText))
        update()
        }
    onSourceEncodingChanged:       {
        // console.debug("updating because sourceEncoding changed to %1"
        //     .arg(sourceEncoding))
        update()
        }
    onDisplayEncodingChanged:      {
        // console.debug("updating because displayEncoding changed to %1"
        //     .arg(displayEncoding))
        update()
    }
}
