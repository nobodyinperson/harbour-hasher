import os
import re
from SCons.Errors import UserError

# Initialize global environment
global_env = Environment(
    PACKAGE_NAME="harbour-hasher",
    TOOLS=["default", "autotools", "git", "textfile"],
    TARFLAGS="-c -z",
)

# Empty install target alias
global_env.Alias("install", [])

# Empty build target alias
global_env.Alias("build", [])

git_version_description = global_env.get("GIT_VERSION_DESCRIPTION")
if git_version_description:
    print("git version description: {}".format(git_version_description))
    global_env["VERSION"] = git_version_description
else:
    version_file = File("VERSION")
    if not os.path.exists(str(version_file)):
        raise UserError(
            "Neither git version nor {} available".format(version_file)
        )
    with open(str(version_file)) as fh:
        global_env["VERSION"] = "-".join(fh)


# Build by default
global_env.Default("build")

Export("global_env")

# Recurse into the subdirectories
nested_sconscript_files = Glob(os.path.join("*", "SConscript"))
# make sure nested_sconscript_files SConscript files are executed in the right
# order
nested_sconscript_files_ordered = sorted(
    nested_sconscript_files,
    key=lambda f: "0"
    if any(x in str(f) for x in ["python", "qml"])
    else str(f),
)
SConscript(nested_sconscript_files_ordered)
