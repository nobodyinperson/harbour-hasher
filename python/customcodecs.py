# system modules
import codecs
import logging
import inspect
import itertools
import functools
import operator
import re
import base64
import math

# internal modules
from . import l10n

# external modules
try:
    import base91
except ImportError:
    base91 = object()


logger = logging.getLogger(__name__)


class CustomCodec(codecs.Codec):
    """
    Base class for custom codecs

    .. note::

        This class works with the following definition:

        - You can **encode** a human-readable sequence of characters (a
          :any:`str`) to get a sequence of :any:`bytes`.
        - The way back is called **decoding**.

        This definition doesn't seem to be the standard. For example, the
        :mod:`base64` module says that :any:`base64.encodebytes` takes a
        (strangely encoded to bytes) string to return a (strangely encoded to
        bytes) bytes Base64 represenation (which should actually be a
        string...)
    """

    registry = {}
    """
    Helper dictionary to realize the search functions handed to
    :any:`codecs.register` without namespace clashes. I didn't get the search
    functions to work properly, even when creating the search functions with
    factory functions. :any:`codecs.lookup` always returned either the first or
    the last :any:`CustomCodec` subclass.
    """

    @classmethod
    def register(cls):
        """
        Add this codec to Python's codec registry
        """
        cls.registry[cls.name] = codecs.CodecInfo(
            name=cls.name, encode=cls.encode, decode=cls.decode
        )
        logger.debug(
            "Registering {cls.displayname} [{cls.name}] codec".format(cls=cls)
        )
        codecs.register(cls.registry.get)

    @classmethod
    def register_subclasses(cls):
        """
        Add all subclasses to Python's codec registry
        """
        for subclass in cls.__subclasses__():
            subclass.register()


intbases = dict(
    zip(
        (2, 8, 10, 16),
        map(
            dict,
            zip(
                zip(
                    itertools.repeat("name"),
                    ("binary", "octal", "decimal", "hexadecimal"),
                ),
                zip(
                    itertools.repeat("translated"),
                    (_("binary"), _("octal"), _("decimal"), _("hexadecimal")),
                ),
                zip(itertools.repeat("formatsymbol"), "bodx"),
            ),
        ),
    )
)


class BasedIntError(ValueError):
    """
    Base class for de-/encoding strings in based-int format
    """

    pass


def basedint2byte(base):
    """
    Create a function to convert a :any:`str` ascii-representation of an
    integer to the base ``base`` to :any:`bytes`.
    """

    def converter(s: str) -> bytes:
        try:
            return bytes((int(s, base),))
        except BaseException as e:
            raise BasedIntError(
                _("Cannot interpret {string} as {type}: {error}").format(
                    string=repr(s), error=e, type=intbases[base]["translated"]
                )
            )

    return converter


def basedintencoder(base: int, sep=False):
    """
    Based on :any:`basedint2byte`, create a wrapper to handle not only single
    numbers, but a series of numbers. Whitespace is interpreted as an optional
    delimiter.
    """
    regex = r"(\S{{1,{}}})\s*".format(
        math.ceil(math.log(256) / math.log(base))
    )
    str2byte = basedint2byte(base)

    def encoderfun(s: str) -> bytes:
        return bytes(
            itertools.chain.from_iterable(
                map(
                    str2byte,
                    map(
                        operator.methodcaller("group", 1),
                        re.finditer(regex, s),
                    ),
                )
            )
        )

    return encoderfun


def basedintdecoder(base: int, sep=False):
    """
    Create a function converting :any:`bytes` to the ascii
    :any:`str`-representation of integers to the base ``base``, optionally
    separating bytes with a space if ``sep`` is ``True``.
    """
    fmt = "{{:0{width}{symbol}}}".format(
        width=math.ceil(math.log(256) / math.log(base)),
        symbol=intbases[base]["formatsymbol"],
    )
    joinstr = " " if sep else ""

    def decoderfun(b: bytes) -> str:
        return joinstr.join(map(fmt.format, b))

    return decoderfun


def make_based_int_codec(base, sep=False):
    """
    Factory function to create a :any:`CustomCodec` subclass handling the
    de-/encoding of :any:`str`/:any:`bytes` ``base``d integers.
    """
    test_data = {
        (2, False): {
            "10110010": b"\xB2",
            "101100100": b"\xB2\x00",
            b"\x08\x12": "0000100000010010",
        },
        (2, True): {
            "10110010": b"\xB2",
            "10110010 0": b"\xB2\x00",
            "1011 0010 0": b"\x0B\x02\x00",
            b"\x08\x12": "00001000 00010010",
        },
        (8, False): {b"\x01\xFF": "001377", "00137": b"\x01\x1F"},
        (8, True): {b"\x01\xFF": "001 377", "001 37 7": b"\x01\x1F\x07"},
        (10, False): {
            b"\xFF\x00\x11": "255000017",
            "2550000170": b"\xFF\x00\x11\x00",
        },
        (10, True): {
            b"\xFF\x00\x11": "255 000 017",
            "255 16 00 170": b"\xFF\x10\x00\xAA",
        },
        (16, False): {
            b"\xFF\x00\x11": "ff0011",
            "afced34a0": b"\xAF\xCE\xD3\x4A\x00",
        },
        (16, True): {
            b"\xFF\x00\x11": "ff 00 11",
            "af c ed 3 4a 0": b"\xAF\x0C\xED\x03\x4a\x00",
        },
    }

    class BasedIntCodec(CustomCodec):
        name = "".join(
            (intbases[base]["name"],) + (("_sep",) if sep else tuple())
        )
        displayname = " - ".join(
            (intbases[base]["translated"],)
            + ((_("separated"),) if sep else tuple())
        )
        bytewise = True
        arbitraryencode = True
        separated = sep
        injective = False

        encoder = basedintencoder(base, sep)
        decoder = basedintdecoder(base, sep)

        @classmethod
        def encode(cls, b: bytes) -> str:
            return (cls.encoder(b), len(b))

        @classmethod
        def decode(cls, s: str) -> bytes:
            return (cls.decoder(s), len(s))

    base_test_data = test_data.get((base, sep))
    if base_test_data:
        BasedIntCodec.test_data = base_test_data

    BasedIntCodec.reciprocal_separation_encoding = (
        re.sub(r"_sep$", "", BasedIntCodec.name)
        if sep
        else (BasedIntCodec.name + "_sep")
    )

    BasedIntCodec.__name__ = "".join(
        map(
            str.title,
            " ".join(
                (intbases[base]["name"],)
                + (("sep",) if sep else tuple())
                + ("codec",)
            ).split(),
        )
    )
    BasedIntCodec.__qualname__ = BasedIntCodec.__name__
    return BasedIntCodec


# Create and register all BasedIntCodecs
for (base, info), sep in itertools.product(intbases.items(), (True, False)):
    cls = make_based_int_codec(base, sep)
    locals()[cls.__name__] = cls
    logger.debug(
        "Created {displayname} [{name}] codec {cls}".format(
            name=repr(cls.name),
            displayname=repr(cls.displayname),
            cls=cls.__name__,
        )
    )


class Utf8Codec(CustomCodec):
    """
    Custom UTF8 Codec
    """

    name = "utf8simple"
    displayname = _("UTF-8")
    bytewise = False
    arbitraryencode = False
    injective = True

    test_data = {"asdf": b"asdf", "äüö": bytes.fromhex("c3a4c3bcc3b6")}

    @staticmethod
    def encode(s: str) -> bytes:
        return (s.encode("utf8"), len(s))

    @staticmethod
    def decode(b: bytes) -> str:
        return (b.decode("utf8"), len(b))


class Base64Codec(CustomCodec):
    """
    Simpler Base64 Codec without line breaks

    .. note::

        Mind the definition difference of encoding/decoding here:

        - :mod:`codec` (and in general), **encoding** means to take a
              :any:`str` and turn it into :any:`bytes`.
        - :func:`base64.encodebytes` does the opposite: it takes arbitrary
            :any:`bytes` and turns them into other :any:`bytes` that can safely
            be turned into :any:`str`!
    """

    name = "base64simple"
    displayname = _("Base64")
    bytewise = False
    arbitraryencode = True
    injective = True

    test_data = {b"asdf": "YXNkZg==", b"\x00\x00\x00": "AAAA"}

    @staticmethod
    def encode(s: str) -> bytes:
        return (base64.decodebytes(s.encode()), len(s))

    @staticmethod
    def decode(b: bytes) -> str:
        return (re.sub(r"\s+", "", base64.encodebytes(b).decode()), len(b))


class Base91Error(ValueError):
    """
    Base class for BasE91 errors
    """


# if base91 is correctly installed
if all(map(functools.partial(hasattr, base91), ("encode", "decode"))):

    class BasE91Codec(CustomCodec):
        """
        Codec for BasE91

        .. note::

            Mind the definition difference of encoding/decoding here:

            - :mod:`codec` (and in general), **encoding** means to take a
                  :any:`str` and turn it into :any:`bytes`.
            - :func:`base91.encode` does the opposite: it takes arbitrary
                :any:`bytes` and turns them into other :any:`bytes` that can
                safely be turned into :any:`str`!
        """

        name = "base91"
        displayname = _("BasE91")
        bytewise = False
        arbitraryencode = True
        injective = True

        test_data = {b"asdf": "v2<1Z"}

        @staticmethod
        def encode(s: str) -> bytes:
            invalid = set(s) - set(base91.base91_alphabet)
            if invalid:
                raise Base91Error(
                    _(
                        "invalid BasE91 characters: {characters}".format(
                            characters="".join(invalid)
                        )
                    )
                )
            return (base91.decode(s), len(s))

        @staticmethod
        def decode(b: bytes) -> str:
            return (base91.encode(b), len(b))


# hook all CustomCodecs into Python's codec registry
CustomCodec.register_subclasses()


def info():
    """
    Yield an information dict for every CustomCodec
    """
    for codec in CustomCodec.__subclasses__():
        # if not getattr(codec, "separated", None):
        yield {
            k: getattr(codec, k, None)
            for k in (
                "name",
                "displayname",
                "arbitraryencode",
                "bytewise",
                "separated",
                "reciprocal_separation_encoding",
            )
        }


def transform(s: str, from_encoding: str, to_encoding: str):
    encoded = codecs.encode(s, from_encoding)
    logger.debug(
        "{} interpreted as {} yields {}".format(
            repr(s), from_encoding, repr(encoded)
        )
    )
    transformed = codecs.decode(encoded, to_encoding)
    logger.debug(
        "{text} transformed from {from_encoding} "
        "to {to_encoding} is {result}".format(
            text=repr(s),
            from_encoding=from_encoding,
            to_encoding=to_encoding,
            result=repr(transformed),
        )
    )
    return transformed
