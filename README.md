[![Hasher on OpenRepos](https://img.shields.io/badge/get%20it-on%20OpenRepos-green.svg)](https://openrepos.net/content/nobodyinperson/hasher) [![latest development RPM](https://img.shields.io/badge/latest%20development%20RPM-on%20GitLab-green.svg)](https://gitlab.com/nobodyinperson/harbour-hasher/-/jobs/artifacts/master/download?job=rpmbuild)

# Hasher

**Hasher** is a [SailfishOS](https://sailfishos.org) application to
manage hash digests.

## Installation

You may install **Hasher** from
[OpenRepos](https://openrepos.net/content/nobodyinperson/hasher) on your
SailfishOS device.

An untested, automatically built RPM from the master branch can be downloaded 
[as an artifact of the CI/CD pipeline](https://gitlab.com/nobodyinperson/harbour-hasher/-/jobs/artifacts/master/browse?job=rpmbuild). The link doesn't work as long as the [Pipeline](https://gitlab.com/nobodyinperson/harbour-hasher/pipelines) on `master` is not finished. Give it a couple of minutes in that case.

## Development

See [`CONTRIBUTING`](CONTRIBUTING.md) for build instructions and developing guidelines.

**Hasher** can be built **without the SailfishOS SDK** on a GNU/Linux machine, you only need a couple of standard tools.
