# Contributing to Hasher

Development follows the [GitLab
Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/).

## Development

### Code Style

Python code should be formatted with [`black`](https://github.com/ambv/black) and
follow [PEP8](https://www.python.org/dev/peps/pep-0008/).

Be sure to have run the following commands before pushing commits. Otherwise,
the CI pipeline will only pass with warnings.

```bash
pip3 install --user black
# format all Python files in the current directory to black style
black -l79 .
```

### Requirements

To build the app, you only need a couple of standard tools. **You don't need
the Sailfish SDK!**. On a Debian-like system, you should be fine installing
these packages (output of snippet $1754230):

```bash
sudo apt update && sudo apt install gawk xz-utils coreutils perl-base rpm gettext inkscape pandoc make findutils openssh-client
```

To set up the build environment, `git clone --recursive` the repository and then run once
from the repository root:

This produces all the necessary Makefiles and the `control` Makefile.

### Building the RPM package

```bash
./build-rpm.sh
```
