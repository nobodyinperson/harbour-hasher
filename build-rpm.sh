#!/bin/bash
set -e

git_working_area_clean()
{
	git diff --quiet
}

git_submodule_update()
{
	git submodule update --init --recursive
}

git_describe_version()
{
	git describe --tags --match 'v*' --dirty --always
}

rpm_version()
{
	git_describe_version | sed -E 's/[^a-zA-Z0-9._]+/+/g'
}

if ! git_working_area_clean;then
	echo >&2 "WARNING: Git working area is not clean"
	# git --no-pager diff
	# exit 1
fi


rpm_dir=rpm

mkdir -p $rpm_dir/{BUILD,BUILDROOT,SOURCES,SPECS,RPMS}

specfile="$rpm_dir/SPECS/harbour-hasher.spec"

rpm_version_str="`rpm_version`"

cat <<EOT > $specfile
# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-hasher
Version: $rpm_version_str
Release: jolla
Summary: Application to handle hash digests
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/harbour-hasher
Source: %{name}-%{version}.tar.gz
Packager: Yann Büchau <nobodyinperson@posteo.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: pandoc
BuildRequires: make
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.4
Requires: sailfishsilica-qt5
Requires: libsailfishapp-launcher
Requires: python3-base
Requires: python3-attrs >= 19.3
Requires: python3-xdgspec >= 0.2
Requires: python3-pwdhash >= 0.1.3
Requires: python3-base91 >= 1.0

%description
Hash calculator and password generator application

%prep
%setup -q -c

%install
scons install --install-sandbox=%{buildroot} --prefix=/usr

%files
%defattr(-,root,root,-)
%{_datadir}/*

EOT

echo "%changelog" >> $specfile

LANG=C git tag -l 'v*' -n99 --sort=-version:refname \
        --format='* %(creatordate:format:%a %b %d %Y) %(taggername) %(taggeremail) - %(refname:strip=2)%0a%(contents)%0a' \
        | perl -pe 's/(\s+)v([\w.]+)/$1$2/g' \
        | perl -ne 'print if not m/^\s*$/g' \
        | perl -pe 's/^(?!\*)\s*(?!-)\s*/- /g' \
        >> $specfile

tarball=harbour-hasher-`rpm_version`.tar.gz

git ls-files --recurse-submodules | xargs tar -czf "$tarball"

mv "$tarball" "$rpm_dir/SOURCES"

rpmbuild -D "_topdir `readlink -f $rpm_dir`" -ba --nodeps "$specfile"
